const { Given, When, Then } = require("cucumber");
const assert = require("assert").strict

Given(/^у дилера на руках "([^"]*)"$/, function (card) {
    this.addCard(card);
});

When(/^он берёт "([^"]*)" из колоды$/, function (card) {
    this.addCard(card);
});

Then(/^не может взять ещё карту$/, function () {
    return this.dealerCanHit().then(
        res => {
            assert.equal(res['can hit'], false);
        }
    );
});

Then(/^может взять ещё карту$/, function () {
    return this.dealerCanHit().then(
        res => {
            assert.equal(res['can hit'], true);
        }
    );

});