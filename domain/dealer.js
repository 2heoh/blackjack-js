const cards = {
    'A': 11,
    'Q': 10,
}

function getValue(card) {
    return cards[card];
}

class Dealer {
    canHit(hand) {

        const value = hand
                        .map( card => getValue(card))
                        .reduce((acc, i) => acc + i)

        return value < 17
    }
}

module.exports = Dealer;