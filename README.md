# A-TDD Kata

Simple node.js server + cucumber.js acceptance tests 

## How to run 
start server on http://localhost:3000/ 
```
npm start
``` 
available path:
```
http://localhost:3000/api/dealer/action?card=A
```
where card - any card in deck from '2' to 'A', can be several:
```
http://localhost:3000/api/dealer/action?card=A&card=Q
```
### returns
```json
{"can hit":true}
```
or 
```json
{"can hit":false}
```


## How to run tests
```
./node_modules/.bin/cucmber-js
```